#include <QCoreApplication>
#include <stdlib.h>
#include <signal.h>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "dbus/player.h"
#include "dbus/playeradapter.h"
#include "dbus/playerclient.h"

void server()
{
    Player* demo = new Player;
    new PlayerAdapter(demo);

    QDBusConnection connection = QDBusConnection::sessionBus();
    connection.registerService("de.kilobyte22.cliplayer");
    connection.registerObject("/", demo);
}

void detach()
{
    qDebug() << "Detaching...";
    setsid();
    int fd;

    fd = open("/dev/null", O_RDWR, 0);

    if (fd != -1)
    {
      dup2 (fd, STDIN_FILENO);
      dup2 (fd, STDOUT_FILENO);
      dup2 (fd, STDERR_FILENO);

      if (fd > 2)
        close (fd);
    }
    else
    {
        std::cerr << QCoreApplication::translate("main", "Could not detach from stdio").toStdString() << std::endl;
    }
}

int checkServer()
{
    QFile file(QString("/tmp/cliplay_") + getenv("USER") + "_pid");
    if (!file.exists())
        return 0;
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    QString contents = in.readAll();
    int pid = contents.toInt();
    // check if process is actually running
    if (!kill(pid, 0))
    {
        // kill successful, kill returned 0 aka false
        qDebug() << "Server running as pid" << pid;
        return pid;
    }
    else
    {
        qDebug() << "Server not running";
        // kill failed, returned -1 aka true
        return 0;
    }
}

void term_handler(int signum)
{
    shutdown(255);
}

void makeServer(QCoreApplication &a)
{
    std::cout << QCoreApplication::translate("main", "Starting server in background...").toStdString() << std::endl;
    // Fork into background and launch daemon there
    int pid;
    if (!(pid = fork()))
    {
        signal(SIGTERM, term_handler);
        qDebug() << "Server thinks pid: " << pid;
        detach(); // detach from stdio
        server();
        exit(a.exec());
    }
    else
    {
        qDebug() << "Client thinks pid: " << pid;
        QFile file(QString("/tmp/cliplay_") + getenv("USER") + "_pid");
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&file);
        out << pid;

        // optional, as QFile destructor will already do it:
        file.close();
        sleep(0.5);
    }
}

void forceArgs(QStringList &args, int required)
{
    int c = args.length() - 1;
    if (c < required)
    {
        std::cerr << QCoreApplication::translate("main", "Invalid argument count. %1 expected, %2 supplied. use -h or --help for help").arg(required).arg(c).toStdString() << std::endl;
        exit(1);
    }
    if (c > required)
    {
        std::cerr << QCoreApplication::translate("main", "%1 unneeded arguments supplied, %2 args wouldve been enough").arg(c - required).arg(required).toStdString() << std::endl;
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription("Command Line DBus powered music player");
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addPositionalArgument("command", QCoreApplication::translate("main", "Command to run. use command 'commands' to list all commands"));

    QCommandLineOption daemonOption(QStringList() << "d" << "daemon", QCoreApplication::translate("main", "Spawn the daemon"));
    parser.addOption(daemonOption);

    QCommandLineOption relativeOption(QStringList() << "r" << "relative", QCoreApplication::translate("main", "Used with command volume to make volume change relative"));
    parser.addOption(daemonOption);

    parser.process(a);

    if (parser.isSet(daemonOption))
    {
        int pid;
        if ((pid = checkServer()))
        {
            std::cerr << QCoreApplication::translate("main", "Server already running. PID is %1").arg(pid).toStdString() << std::endl;
        }
        else
        {
            makeServer(a);
        }
    }

    if (!checkServer())
    {
        std::cerr << QCoreApplication::translate("main", "Server not running, start it with -d or --daemon").toStdString() << std::endl;
        exit(1);
    }
    else
    {
        qDebug() << "Server running";
    }

    PlayerClient client;

    QStringList args = parser.positionalArguments();

    if (args.length() == 0)
        exit(0);
    QString call = args[0];

    if (call == "play")
    {
        forceArgs(args, 2);
        client.play(args[1], args[2]);
    }
    else if (call == "stop")
    {
        forceArgs(args, 0);
        client.stop();
    }
    else if (call == "pause")
    {
        forceArgs(args, 0);
        client.pause();
    }
    else if (call == "resume")
    {
        forceArgs(args, 0);
        client.resume();
    }
    else if (call == "quit")
    {
        forceArgs(args, 0);
        client.quit(0);
    }
    else if (call == "restart")
    {
        forceArgs(args, 0);
        client.quit(0);
        sleep(0.5);
        makeServer(a);
    }
    else if (call == "volume")
    {
        forceArgs(args, 1);
        QString arg = args[1];
        bool rel = parser.isSet(relativeOption);

        client.setVolume(rel, (arg.toDouble() / 100));
    }
    else if (call == "commands")
    {
        forceArgs(args, 0);
        QStringList strings;
        strings << "play file|url <path>" << "stop" << "pause" << "resume" << "quit" << "restart" << "volume [-r] <volume>";
        std::cout << strings.join("\n").toStdString() << std::endl;
    }
    else
    {
        std::cerr << QCoreApplication::translate("main", "Invalid call").toStdString() << std::endl;
        exit(1);
    }
    return 0;
}
