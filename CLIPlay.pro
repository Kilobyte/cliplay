#-------------------------------------------------
#
# Project created by QtCreator 2014-01-05T19:39:35
#
#-------------------------------------------------

QT       += core dbus

QT       -= gui

TARGET = CLIPlay
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    dbus/player.cpp \
    dbus/playeradapter.cpp \
    dbus/playerinterface.cpp \
    dbus/playerclient.cpp

HEADERS += \
    dbus/player.h \
    dbus/playeradapter.h \
    dbus/playerinterface.h \
    dbus/playerclient.h

OTHER_FILES += \
    dbus/playeradapter.xml

unix|win32: LIBS += -L$$PWD/lib/ -lbass

INCLUDEPATH += $$PWD/lib
DEPENDPATH += $$PWD/lib
