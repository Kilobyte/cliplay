#include "player.h"
#include <iostream>
#include "lib/bass.h"
#include <QFile>

Player::Player(QObject *parent) :
    QObject(parent)
{
    void *handle = 0;
    void *guid = 0;
    BASS_Init(-1, 44100, 0, handle, guid);
    stream = 0;
}

void Player::quit(int exitcode)
{
    shutdown(exitcode);
}

double Player::getTimestamp()
{
    QWORD len = BASS_ChannelGetPosition(stream, BASS_POS_BYTE); // the length in bytes
    return BASS_ChannelBytes2Seconds(stream, len);
}

bool Player::pause()
{
    std::cout << "Paused." << std::endl;
    if (paused || !stream)
        return false;
    BASS_ChannelStop(stream);
    paused = true;
    return true;
}

bool Player::resume()
{
    if (!(paused && stream))
        return false;
    BASS_ChannelPlay(stream, false);
    paused = false;
    return true;
}

bool Player::play(const QString &type, const QString &path)
{
    std::cout << QString("Played %1 as %2").arg(path).arg(type).toStdString() << std::endl;
    if (stream)
        stop();
    if (type == "file") {
        stream = BASS_StreamCreateFile(false, path.toStdString().c_str(), 0, 0, 0);
    } else if (type == "url") {
        stream = BASS_StreamCreateURL(path.toStdString().c_str(), 0, 0, 0, 0);
    }
    if (!stream)
    {
        std::cerr << "Bass Error: " << BASS_ErrorGetCode() << std::endl;
        return false;
    }
    BASS_ChannelPlay(stream, false);
    return true;
}

bool Player::stop()
{
    std::cout << "Stopped." << std::endl;
    if (!stream)
        return false;
    BASS_ChannelStop(stream);
    BASS_StreamFree(stream);
    stream = 0;
    paused = false;
    return true;
}

double Player::getLength()
{

    QWORD len = BASS_ChannelGetLength(stream, BASS_POS_BYTE); // the length in bytes
    return BASS_ChannelBytes2Seconds(stream, len);
}

double Player::getVolume()
{
    return BASS_GetVolume();
}

void Player::setVolume(bool relative, double volume)
{
    double vol = 0;
    if (relative)
    {
        vol = getVolume();
    }
    vol += volume;
    if (vol < 0)
        vol = 0;
    if (vol > 1)
        vol = 1;
    BASS_SetVolume(vol);
}


void shutdown(int code)
{
    QFile file(QString("/tmp/cliplay_") + getenv("USER") + "_pid");
    if (file.exists())
        file.remove();
    exit(code);
}
