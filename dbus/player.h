#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>

class Player : public QObject
{
    Q_OBJECT
public:
    explicit Player(QObject *parent = 0);

public: // PROPERTIES
public Q_SLOTS: // METHODS
    void quit(int exitcode);
    double getTimestamp();
    bool pause();
    bool resume();
    bool play(const QString &type, const QString &path);
    bool stop();
    double getLength();
    double getVolume();
    void setVolume(bool relative, double volume);
private:
    int stream;
    bool paused;
};

void shutdown(int);

#endif // PLAYER_H
