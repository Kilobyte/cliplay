#include "playerclient.h"
#include <QtDBus/QDBusConnection>


PlayerClient::PlayerClient(QObject *parent) :
    QObject(parent)
{

}

QDBusMessage PlayerClient::makeMessage(QString method)
{
    return QDBusMessage::createMethodCall("de.kilobyte22.cliplayer", "/", "", method);
}

void PlayerClient::quit(int exitcode)
{
    QDBusMessage m = makeMessage("quit");
    m << exitcode;
    bool queued = QDBusConnection::sessionBus().send(m);
}

double PlayerClient::getTimestamp()
{
    QDBusMessage m = makeMessage("getTimestamp");
    bool queued = QDBusConnection::sessionBus().send(m);
    return 0;
}

bool PlayerClient::pause()
{
    QDBusMessage m = makeMessage("pause");
    bool queued = QDBusConnection::sessionBus().send(m);
    return true;
}

bool PlayerClient::resume()
{
    QDBusMessage m = makeMessage("resume");
    bool queued = QDBusConnection::sessionBus().send(m);
    return true;
}

bool PlayerClient::play(const QString &type, const QString &path)
{
    QDBusMessage m = makeMessage("play");
    m << type;
    m << path;
    bool queued = QDBusConnection::sessionBus().send(m);
    return true;
}

bool PlayerClient::stop()
{
    QDBusMessage m = makeMessage("stop");
    bool queued = QDBusConnection::sessionBus().send(m);
    return true;
}

double PlayerClient::getLength()
{
    return 0;
}

double PlayerClient::getVolume()
{
    QDBusMessage m = makeMessage("getVolume");
    QDBusConnection::sessionBus().send(m);
    return true;
    return 0;
}

void PlayerClient::setVolume(bool relative, double volume)
{
    QDBusMessage m = makeMessage("setVolume");
    m << relative;
    m << volume;
    QDBusConnection::sessionBus().send(m);
}



