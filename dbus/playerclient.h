#ifndef PLAYERCLIENT_H
#define PLAYERCLIENT_H

#include <QObject>
#include <QtDBus/QDBusMessage>

class PlayerClient : public QObject
{
    Q_OBJECT
public:
    explicit PlayerClient(QObject *parent = 0);
    void quit(int exitcode);
    double getTimestamp();
    bool pause();
    bool resume();
    bool play(const QString &type, const QString &path);
    bool stop();
    double getLength();
    double getVolume();
    void setVolume(bool relative, double volume);
signals:

public slots:

private:
    QDBusMessage makeMessage(QString method);

};

#endif // PLAYERCLIENT_H
